year = int(input("Please input a year:\n "))
if (year % 400 == 0) and (year % 100 == 0):
    print("{0} is a leap year".format(year))
elif (year % 4 ==0) and (year % 100 != 0):
    print("{0} is a leap year.".format(year))
else:
    print("{0} is not a leap year.".format(year))

rows = int(input("Please enter the amount of rows in your grid:"))

columns = int(input("Please enter the amount of columns in your grid:"))

r=1
while r <= rows:
    c=1
    while c <= columns:
        print("*" , end =" ")
        c+=1
    print()
    r+=1